-- Copyright (c) 2024 Raptor Engineering, LLC
-- Licensed under the terms of the GNU LGPLv3
-- Algorithm based on "A Simple PLL-Based True Random Number Generator for Embedded Digital Systems"
--
-- WARNING: The output of this generator has NOT been tested for randomness!
-- WARNING: Do not use for secure applications until suitable tests have been performed!
--
-- In particular, the number of shift register stages required to reduce bias to an acceptable level
-- is currently unknown...

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity random is
    port (
        clk  : in std_ulogic;
        pll  : in std_ulogic;
        data : out std_ulogic_vector(63 downto 0);
        raw  : out std_ulogic_vector(63 downto 0);
        err  : out std_ulogic := '1'
        );
end entity random;

architecture behaviour of random is
        signal pll_rand1     : std_ulogic;
        signal pll_rand2     : std_ulogic;
        signal decimator     : std_ulogic_vector(15 downto 0);
        signal decimator_out : std_ulogic;
        signal decimator_des : std_ulogic_vector(63 downto 0);
        signal startup_delay : std_ulogic_vector(7 downto 0) := (others => '0');
        signal lhca          : std_ulogic_vector(63 downto 0);

        constant lhca_diag   : std_ulogic_vector(63 downto 0) := x"fffffffffffffffb";
begin
    process(clk)
    begin
        if rising_edge(clk) then
            pll_rand1 <= pll;
            pll_rand2 <= pll_rand1;

            decimator(15) <= decimator(14);
            decimator(14) <= decimator(13);
            decimator(13) <= decimator(12);
            decimator(12) <= decimator(11);
            decimator(11) <= decimator(10);
            decimator(10) <= decimator(9);
            decimator(9) <= decimator(8);
            decimator(8) <= decimator(7);
            decimator(7) <= decimator(6);
            decimator(6) <= decimator(5);
            decimator(5) <= decimator(4);
            decimator(4) <= decimator(3);
            decimator(3) <= decimator(2);
            decimator(2) <= decimator(1);
            decimator(1) <= decimator(0);
            decimator(0) <= pll_rand2;

            decimator_out <= decimator(0) xor
                             decimator(1) xor
                             decimator(2) xor
                             decimator(3) xor
                             decimator(4) xor
                             decimator(5) xor
                             decimator(6) xor
                             decimator(7) xor
                             decimator(8) xor
                             decimator(9) xor
                             decimator(10) xor
                             decimator(11) xor
                             decimator(12) xor
                             decimator(13) xor
                             decimator(14) xor
                             decimator(15);

            decimator_des(63) <= decimator_des(62);
            decimator_des(62) <= decimator_des(61);
            decimator_des(61) <= decimator_des(60);
            decimator_des(60) <= decimator_des(59);
            decimator_des(59) <= decimator_des(58);
            decimator_des(58) <= decimator_des(57);
            decimator_des(57) <= decimator_des(56);
            decimator_des(56) <= decimator_des(55);
            decimator_des(55) <= decimator_des(54);
            decimator_des(54) <= decimator_des(53);
            decimator_des(53) <= decimator_des(52);
            decimator_des(52) <= decimator_des(51);
            decimator_des(51) <= decimator_des(50);
            decimator_des(50) <= decimator_des(49);
            decimator_des(49) <= decimator_des(48);
            decimator_des(48) <= decimator_des(47);
            decimator_des(47) <= decimator_des(46);
            decimator_des(46) <= decimator_des(45);
            decimator_des(45) <= decimator_des(44);
            decimator_des(44) <= decimator_des(43);
            decimator_des(43) <= decimator_des(42);
            decimator_des(42) <= decimator_des(41);
            decimator_des(41) <= decimator_des(40);
            decimator_des(40) <= decimator_des(39);
            decimator_des(39) <= decimator_des(38);
            decimator_des(38) <= decimator_des(37);
            decimator_des(37) <= decimator_des(36);
            decimator_des(36) <= decimator_des(35);
            decimator_des(35) <= decimator_des(34);
            decimator_des(34) <= decimator_des(33);
            decimator_des(33) <= decimator_des(32);
            decimator_des(32) <= decimator_des(31);
            decimator_des(31) <= decimator_des(30);
            decimator_des(30) <= decimator_des(29);
            decimator_des(29) <= decimator_des(28);
            decimator_des(28) <= decimator_des(27);
            decimator_des(27) <= decimator_des(26);
            decimator_des(26) <= decimator_des(25);
            decimator_des(25) <= decimator_des(24);
            decimator_des(24) <= decimator_des(23);
            decimator_des(23) <= decimator_des(22);
            decimator_des(22) <= decimator_des(21);
            decimator_des(21) <= decimator_des(20);
            decimator_des(20) <= decimator_des(19);
            decimator_des(19) <= decimator_des(18);
            decimator_des(18) <= decimator_des(17);
            decimator_des(17) <= decimator_des(16);
            decimator_des(16) <= decimator_des(15);
            decimator_des(15) <= decimator_des(14);
            decimator_des(14) <= decimator_des(13);
            decimator_des(13) <= decimator_des(12);
            decimator_des(12) <= decimator_des(11);
            decimator_des(11) <= decimator_des(10);
            decimator_des(10) <= decimator_des(9);
            decimator_des(9) <= decimator_des(8);
            decimator_des(8) <= decimator_des(7);
            decimator_des(7) <= decimator_des(6);
            decimator_des(6) <= decimator_des(5);
            decimator_des(5) <= decimator_des(4);
            decimator_des(4) <= decimator_des(3);
            decimator_des(3) <= decimator_des(2);
            decimator_des(2) <= decimator_des(1);
            decimator_des(1) <= decimator_des(0);
            decimator_des(0) <= decimator_out;

            raw <= decimator_des;

            -- linear hybrid cellular automaton
            -- used to even out the statistics of the ring oscillators
            lhca <= ('0' & lhca(63 downto 1)) xor (lhca and lhca_diag) xor
                    (lhca(62 downto 0) & '0') xor decimator_des;

            data <= lhca;
        end if;
        if rising_edge(clk) then
            if startup_delay > 100 then
                startup_delay <= startup_delay + 1;
            else
                err <= '0';
            end if;
        end if;
    end process;
end behaviour;
